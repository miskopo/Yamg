from enum import Enum


class Colors(Enum):
    # Basic colors
    BLACK = (0, 0, 0)
    WHITE = (255, 255, 255)
    RED = (255, 0, 0)
    GREEN = (0, 255, 0)
    BLUE = (0, 0, 255)
    YELLOW = (255, 255, 0)
    VIOLET = (255, 0, 255)
    # Darker colors
    GREY = (100, 100, 100)
    DARK_RED = (100, 0, 0)
    DARK_GREEN = (0, 100, 0)
    DARK_BLUE = (0, 0, 100)
    DARK_YELLOW = (100, 100, 0)
    DARK_VIOLET = (100, 0, 100)
