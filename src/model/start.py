import logging
import pygame
from pygbutton import PygButton
from src.common.colors import Colors
from src.model.elements.background import Background

logger = logging.getLogger()


class Start:
    # TODO: Tidy up
    def __init__(self, surface):
        logger.debug("Initiating start screen")
        self.surface = surface
        self.draw_start_screen()

    def draw_start_screen(self):
        logger.debug("Filling screen white")
        self.surface.fill(Colors.WHITE.value)
        logger.debug("Setting background image")
        background = Background('src/static/background/start_bg.png', [0, 0], self.surface)
        self.surface.blit(background.image, background.rect)
        logger.debug("Adding masive game logo")
        font = pygame.font.SysFont('Arial', self.surface.get_rect().height // 2)
        title = font.render("Yamg!", True, Colors.WHITE.value)
        self.surface.blit(title,
                          ((self.surface.get_rect().width - title.get_rect().width) / 2,
                           self.surface.get_rect().height * 0.05,
                           self.surface.get_rect().height / 2,
                           self.surface.get_rect().width))
        logger.debug("Adding start game button")
        self.start_button = PygButton((self.surface.get_rect().centerx - 150, self.surface.get_rect().centery, 300, 80),
                                      'Start Game',
                                      bgcolor=Colors.DARK_YELLOW.value,
                                      fgcolor=Colors.WHITE.value)
        self.start_button.draw(self.surface)

    def get_start_button(self):
        return self.start_button
