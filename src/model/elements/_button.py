import pygame


class _Button:
    """
    Class representing clickable button
    """ 
    def __init__(self, text, bg_color, text_color, surface, size, position, font_face='Arial', font_size=False):
        if not font_size:
            font = pygame.font.SysFont(font_face, int(size[1] * 0.95))
        else:
            font = pygame.font.SysFont(font_face, font_size)
        coordinates = (position[0], position[1], position[0] + size[0], position[1] + size[1])
        pygame.draw.rect(surface, bg_color, coordinates)
        surface.blit(font.render(text, True, text_color), (position[0] + size[1] * 0.1, position[1] + size[1] * 0.45))



