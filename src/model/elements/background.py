import pygame


class Background(pygame.sprite.Sprite):
    def __init__(self, image_file, location, surface=None):
        pygame.sprite.Sprite.__init__(self)  # call Sprite initializer
        self.image = pygame.image.load(image_file)
        self.rect = self.image.get_rect()
        if surface is not None:
            # TODO: Fix scaling
            pygame.transform.scale(self.image, (surface.get_rect().width, surface.get_rect().height))
            self.rect.left = surface.get_rect().left
            self.rect.right = surface.get_rect().right
        self.rect.left, self.rect.top = location
