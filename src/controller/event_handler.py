import pygame
import logging
from src.model.start import Start

logger = logging.getLogger()


class EventHandler:
    def controller_tick(self, buttons):
        for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    logger.debug("Exit button pressed, exiting")
                    exit(0)
                if 'click' in buttons['start_button'].handleEvent(event):
                    logger.debug("Start button pressed")

