import pygame

WINDOW_NAME = "YAMG!"


class WindowHandler:
    def __init__(self, resolution=(1280, 960)):
        self.display = pygame.display
        self.resolution = resolution

    def __call__(self, *args, **kwargs):
        self.create_window()

    def create_window(self):
        self.display.set_mode(self.resolution, pygame.RESIZABLE)
        self.display.set_caption(WINDOW_NAME)

    def view_tick(self):
        self.display.flip()