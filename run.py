import pygame
import logging
from src.controller.window_handler import WindowHandler
from src.controller.event_handler import EventHandler
from src.model.start import Start
from src.model.finish import Finish

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()
handler = logging.FileHandler('logs/debug.log')
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

if __name__ == '__main__':
    pygame.init()
    window = WindowHandler()
    event_handler = EventHandler()
    logger.debug("Initiating window")
    window()
    buttons = {}
    start_screen = Start(window.display.get_surface())
    buttons['start_button'] = start_screen.get_start_button()
    while True:
        window.view_tick()
        event_handler.controller_tick(buttons)
