# YAMG!
Yet Another Mario game! :running:

[![Build Status](https://travis-ci.org/miskopo/Yamg.svg?branch=master)](https://travis-ci.org/miskopo/Yamg) [![codecov](https://codecov.io/gh/miskopo/Yamg/branch/master/graph/badge.svg)](https://codecov.io/gh/miskopo/Yamg)

This 2D platform game is somewhat clone of famous Mario, however with perks and stuff not yet to be seen in whole universe. Well, maybe behind the final frontier.
It's implemented in :snake: using pygame module.


## DISCLAIMER
This project is in its beginning phase. Please, be patient :)

## Progress
- [ ] Create basic game interface (loading screen, menu etc)
- [ ] Create characters and background graphic
- [ ] Implement movement
- [ ] Implement obstacle generation
- [ ] Hide Easter eggs 
- [ ] ???
- [ ] Profit



## Installation
__Sorry, not yet applicable__

## Contributors
@miskopo
@cathelyn

